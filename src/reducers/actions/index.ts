import axios from "axios";

import { IUser } from "src/interfaces";
import { baseUrl, delay, getUserPosts } from "src/helpers";
import {
	SET_USERS,
	UPDATE_LOADING,
	UPDATE_USER,
	SET_USER,
	CLEAR_USER,
} from "src/constants/action-types";

/**
 * Update loading
 * @param key
 * @param value
 */
export const updateLoading = (key: string, value: boolean) => ({
	type: UPDATE_LOADING,
	payload: {
		key,
		value,
	},
});

/**
 * Set current user with his posts
 * @param userId
 */
export const setUser = (userId: number) => async (dispatch: any) => {
	dispatch({
		type: SET_USER,
		payload: { user: { id: userId }, posts: await getUserPosts(userId) },
	});
	await delay(1000);
	dispatch(updateLoading("user", false));
};

/**
 * Fetch user(s) data
 * @param userId
 */
export const getUsers = (userId?: number) => async (dispatch: any) => {
	const response = await axios.get<IUser[]>(`${baseUrl}users`);
	const users: IUser[] = response.data;

	dispatch({
		type: SET_USERS,
		payload: { users },
	});

	if (userId) {
		dispatch(setUser(userId));
	} else {
		await delay(1000);
		dispatch(updateLoading("users", false));
	}
};

/**
 * Clear user variable in store
 */
export const clearUser = () => ({
	type: CLEAR_USER,
});

/**
 * Update user data
 * @param user
 */
export const updateUser = (user: IUser) => async (dispatch: any) => {
	dispatch({
		type: UPDATE_USER,
		payload: { user },
	});
	await delay(1000);
	dispatch(updateLoading("users", false));
};
