import { createRoot } from "react-dom/client";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./reducers/store";

import App from "./components/App";

import "./index.scss";

const router = createBrowserRouter([
	{
		path: "*",
		element: <App />,
	},
]);

const container = document.getElementById("root");
const root = createRoot(container as Element);

root.render(
	<Provider store={store}>
		<RouterProvider router={router} />
	</Provider>
);
