import { NavLink } from "react-router-dom";

import Breadcrumbs from "../Breadcrumb/Breadcrumb";

import "./Navbar.scss";

function Navbar() {
	return (
		<>
			<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
				<div className="container-fluid">
					<button
						className="navbar-toggler"
						type="button"
						data-bs-toggle="collapse"
						data-bs-target="#navbarSupportedContent"
						aria-controls="navbarSupportedContent"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon" />
					</button>
					<div className="collapse navbar-collapse" id="navbarSupportedContent">
						<ul className="navbar-nav me-auto mb-2 mb-lg-0">
							<li className="nav-item">
								<NavLink to="/" className="nav-link">
									Accueil
								</NavLink>
							</li>
							<li className="nav-item">
								<NavLink to="/users" className="nav-link">
									Utilisateurs
								</NavLink>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<div className="container">
				<div id="breadcrumb">
					<Breadcrumbs />
				</div>
			</div>
		</>
	);
}

export default Navbar;
