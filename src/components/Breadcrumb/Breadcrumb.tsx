import { Link } from "react-router-dom";
import useBreadcrumbs from "use-react-router-breadcrumbs";

function DynamicUserBreadcrumb({ match }) {
	const name: string[] = match.params.name.split("-");

	return <span>{name.join(" ")}</span>;
}

function Breadcrumbs() {
	const breadcrumbs = useBreadcrumbs([
		{ path: "/", breadcrumb: "Accueil" },
		{ path: `/users`, breadcrumb: "Utilisateurs" },
		{ path: `/users/:id/:name`, breadcrumb: DynamicUserBreadcrumb },
	]);

	return (
		<nav aria-label="breadcrumb">
			<ol className="breadcrumb">
				{breadcrumbs.map(({ match, breadcrumb }) => {
					const hasParams = Object.keys(match.params).length >= 0;

					if (!hasParams) {
						return (
							<li key={match.pathname} className="breadcrumb-item">
								<Link to={`/${match.pathname}`}>{breadcrumb}</Link>
							</li>
						);
					}

					return (
						<li key={match.pathname} className="breadcrumb-item">
							<Link to={match.pathname}>{breadcrumb}</Link>
						</li>
					);
				})}
			</ol>
		</nav>
	);
}

export default Breadcrumbs;
