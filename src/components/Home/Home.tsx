import { Link } from "react-router-dom";

import "./Home.scss";

function Home() {
	return (
		<section id="hero" className="d-flex align-items-center">
			<div className="container position-relative" data-aos="fade-up" data-aos-delay="100">
				<div className="row justify-content-center">
					<div className="col-xl-7 col-lg-9 text-center">
						<h1>JSONPlaceholder</h1>
					</div>
				</div>
				<div className="text-center">
					<Link className="btn-get-started navbar-brand" to="/users">
						AFFICHER LES UTILISATEURS
					</Link>
				</div>
			</div>
		</section>
	);
}

export default Home;
