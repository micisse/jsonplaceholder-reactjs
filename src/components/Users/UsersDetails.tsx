import { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import ClipLoader from "react-spinners/ClipLoader";
import { AnyAction } from "redux";
import { useParams } from "react-router-dom";

import { clearUser, getUsers, setUser, updateLoading } from "src/reducers/actions";
import { initTrigger, jsUcfirst, override, truncate } from "src/helpers";
import { IPost, IState } from "src/interfaces";

import "./Users.scss";

const UserDetails = (props: any) => {
	const { loading, user, users } = props as IState;
	const dispatch = useDispatch();
	const params = useParams();

	useEffect(() => {
		const id = parseInt(params.id as string);

		if (users.length === 0) dispatch(getUsers(id) as unknown as AnyAction);
		else dispatch(setUser(id) as unknown as AnyAction);

		return () => {
			dispatch(updateLoading("user", true));
			dispatch(clearUser());
		};
	}, []);

	if (loading.user) return <ClipLoader loading={loading.user} cssOverride={override} size={50} />;

	initTrigger();

	return (
		<div>
			<div className="row">
				<div className="table-responsive">
					<table className="table table-striped user-details">
						<tbody>
							<tr>
								<td>
									<div>
										<b>Nom d'utilisateur</b>: <br /> {user.username}
									</div>
								</td>
								<td>
									<div>
										<b>Email</b>: <br /> {user.email}
									</div>
								</td>
							</tr>
						</tbody>
						<tbody>
							<tr>
								<td>
									<div>
										<b>Ville</b>: <br /> {user.address.city}
									</div>
								</td>
								<td>
									<div>
										<b>Suite</b>: <br /> {user.address.suite}
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div className="posts">
				<div className="title-sub">
					<h4>
						<b>POSTS</b>
					</h4>
				</div>

				<div className="row">
					{user.posts ? (
						user.posts.map((post: IPost, i: number) => (
							<div key={post.id} className="col-md-4">
								<div className="card">
									<img
										src={`https://picsum.photos/id/${i + 1}/200`}
										className="card-img-top"
										alt={post.title}
									/>
									<div className="card-body">
										<h5
											data-bs-toggle="tooltip"
											data-bs-placement="top"
											title={post.title}
											className="card-title"
										>
											{truncate(jsUcfirst(post.title), 3, "...")}
										</h5>
										<p
											data-bs-toggle="tooltip"
											data-bs-placement="top"
											title={post.body}
											className="card-text"
										>
											{truncate(jsUcfirst(post.body), 10, "...")}
										</p>
									</div>
								</div>
							</div>
						))
					) : (
						<div className="text-center"> Aucun posts pour cet utilisateur </div>
					)}
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = (state: IState) => ({
	loading: state.loading,
	user: state.user,
	users: state.users,
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
