export const stats: string;
export const usersTable: string;
export const userDetails: string;
export const posts: string;
export const titleSub: string;
export const card: string;
export const cardTitle: string;
export const cardText: string;
