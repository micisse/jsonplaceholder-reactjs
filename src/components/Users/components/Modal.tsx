import { FC } from "react";

import { IUser } from "../../../interfaces";

const Modal: FC<{
	user: IUser;
	handleUpdate: () => void;
	// eslint-disable-next-line no-unused-vars
	handleChange: (evt: any) => void;
	values: any;
}> = (props) => {
	const { user, values, handleUpdate, handleChange } = props;

	return (
		<div
			className="modal fade"
			id="editModal"
			data-backdrop="false"
			aria-labelledby="userModalLabel"
			aria-hidden="true"
		>
			<div className="modal-dialog">
				<div className="modal-content">
					<div className="modal-header">
						<h5 className="modal-title" id="userModalLabel">
							Modifier l'utilisateur {user.name}
						</h5>
						<button
							type="button"
							className="btn-close"
							data-bs-dismiss="modal"
							aria-label="Close"
						/>
					</div>
					<div className="modal-body">
						<div className="input-group mt-4 mb-4">
							<span className="input-group-text" id="inputGroup-sizing-sm">
								Nom
							</span>
							<input
								value={values.name || ""}
								onChange={handleChange}
								type="text"
								name="name"
								aria-label="Nom"
								placeholder="Nom"
								className="form-control"
							/>
						</div>
						<div className="input-group mt-4 mb-4">
							<span className="input-group-text" id="inputGroup-sizing-sm">
								Nom d'utilisateur
							</span>
							<input
								value={values.username || ""}
								onChange={handleChange}
								type="text"
								name="username"
								aria-label="Nom d'utilisateur"
								placeholder="Nom d'utilisateur"
								className="form-control"
							/>
						</div>
					</div>
					<div className="modal-footer">
						<button type="button" className="btn btn-secondary" data-bs-dismiss="modal">
							Fermer
						</button>
						<button type="button" onClick={handleUpdate} className="btn btn-warning">
							Modifier
						</button>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Modal;
