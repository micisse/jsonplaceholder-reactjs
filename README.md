# JSONPlaceholder

### PREREQUISITES

- **NodeJS**: *18.12.1*
- **Yarn**: *1.22.19*
- Or **NPM**: *8.19.2*

## STACK

ReactJS 18.2.0, React-Router-Dom, Redux, Redux-Thunk, Axios, Typescript, SCSS, Webpack [...]

## API

> https://jsonplaceholder.typicode.com

## GOAL

Small webapp displaying users and their related posts (data are located on [jsonplaceholder](https://jsonplaceholder.typicode.com)).

#### Tasks
---
-   [x] Init project & adding Webpack (Babel, Eslint, Prettier, ...) - Dev & Build env preparation
-   [x] Adding react-router
-   [x] Adding bootstrap (CDN)
-   [x] Adding redux
---
-   [x] Fetch users on [jsonplaceholder](https://jsonplaceholder.typicode.com) and display them in table.
-   [x] Add a spinner while information is loading
-   [x] Display user details with posts - Split the application into different pages (with react-router)
-   [x] The name of each user should be editable, and should send the update to the server (note that the update is faked on jsonplaceholder).
-   [x] Display the number of users living in an Appt, and in a Suite (field address.suite)

#### Dev

```sh
yarn start (or npm run start)
```

#### Prod

```sh
yarn build (or npm run build)
```

### Lint and lint fix

```sh
yarn lint (or npm run lint)
yarn lint:fix (or npm run lint:fix)
```
